
use std::env;
use std::path::{Path, PathBuf};

fn main() {
    let pwd_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let path = Path::new(&*pwd_dir).join("lib");
    println!("cargo:rustc-link-search=native={}", path.to_str().unwrap());
    println!("cargo:rustc-link-lib=dylib=cryptoauth");
    // println!("cargo:rustc-link-lib=static=add");
    // println!("cargo:rerun-if-changed=src/hello.c");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=wrapper.h");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        // .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .clang_arg("-I/usr/include/cryptoauthlib/")
        .clang_arg("-I./build_includes")
        // Check if thes eare necessary....
        .clang_arg("-DATCA_HAL_I2C=ON")
        .clang_arg("-DATCA_PKCS11=ON")
        .clang_arg("-DATCA_ATECC608A_SUPPORT=ON")
        .clang_arg("-DATCA_OPENSSL=ON")
        .clang_arg("-DATCA_USE_ATCAB_FUNCTIONS=ON")
        // .formatter(bindgen::Formatter::Rustfmt)
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    println!("{}", out_path.join("bindings.rs").display());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

}

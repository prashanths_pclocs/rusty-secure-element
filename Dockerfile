FROM rust

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y cmake gcc libclang1 clang

ENV CRYPTOAUTHLIB_VERSION=3.3.0

RUN wget https://github.com/MicrochipTech/cryptoauthlib/archive/refs/tags/v${CRYPTOAUTHLIB_VERSION}.tar.gz -O - | tar -xz \
  && cd cryptoauthlib-${CRYPTOAUTHLIB_VERSION} \
  && cmake -DATCA_HAL_I2C=ON -DATCA_PKCS11=ON -DATCA_ATECC608A_SUPPORT=ON -DATCA_OPENSSL=ON -B build \
  && cmake --build build --target install


use openssl::{
  x509::{X509, X509NameBuilder, X509Builder, X509Req, extension::{SubjectKeyIdentifier, AuthorityKeyIdentifier}},
  ec::{EcKey, PointConversionForm, EcGroup},
  asn1::Asn1Time,
  pkey::{PKey, Private, Public},
  hash::{hash, MessageDigest},
  bn::{BigNum, BigNumContext},
  nid::Nid
};
use chrono::prelude::*;
use chrono::{DateTime, NaiveDateTime, Utc};
use std::mem::transmute;


const EXPIRE_YEAR: i32 = 17;


pub fn generate_cert(
  thing_name: &str,
  csr: X509Req,
  ca_crt: X509,
  ca_key: EcKey<Private>,
) -> X509 {

  let mut builder = X509Builder::new().unwrap();
  builder.set_version(0x02).unwrap();


  let mut x509_name = X509NameBuilder::new().unwrap();
  x509_name.append_entry_by_text("CN", thing_name).unwrap();
  builder.set_subject_name(&x509_name.build()).unwrap();

  builder.set_issuer_name(ca_crt.subject_name()).unwrap();


  let not_before = DateTime::<Utc>::from_local(NaiveDateTime::new(
    Utc::now().date_naive(),
    NaiveTime::from_hms_milli_opt(12, 0, 0, 0).unwrap()
  ), Utc);
  builder.set_not_before(&Asn1Time::from_str(format!("{}", not_before.format("%y%m%d%H%M%SZ")).as_str()).unwrap().as_ref()).unwrap();

  let not_after = not_before.with_year(not_before.year() + EXPIRE_YEAR).unwrap();
  builder.set_not_after(&Asn1Time::from_str(format!("{}", not_after.format("%y%m%d%H%M%SZ")).as_str()).unwrap().as_ref()).unwrap();

  let serial_number = generate_cert_sn(&csr.public_key().unwrap(), not_before);
  builder.set_serial_number(serial_number.to_asn1_integer().unwrap().as_ref()).unwrap();

  builder.append_extension(
    SubjectKeyIdentifier::default()
    .build(&builder.x509v3_context(Some(ca_crt.as_ref()), None)).
    unwrap()
  ).unwrap();

  builder.append_extension(
    AuthorityKeyIdentifier::default()
      .keyid(true)
      .issuer(false)
      .build(&builder.x509v3_context(Some(ca_crt.as_ref()), None))
      .unwrap()
  ).unwrap();

  builder.set_pubkey(&csr.public_key().unwrap()).unwrap();

  let ca_pkey = PKey::from_ec_key(ca_key).unwrap();
  builder.sign(ca_pkey.as_ref(), MessageDigest::sha256()).unwrap();

  builder.build()

}


// Generates a serial number that is expected by SNSRC_PUB_KEY_HASH
// Refer to https://ww1.microchip.com/downloads/en/Appnotes/20006367A.pdf
fn generate_cert_sn(pub_key: &PKey<Public>, not_before: DateTime::<Utc>) -> BigNum {

  let ec_key = pub_key.ec_key().unwrap();
  let ec_pubkey = ec_key.public_key();


  let nid = Nid::X9_62_PRIME256V1; // NIST P-256 curve
  let group = EcGroup::from_curve_name(nid).unwrap();
  let mut ctx = BigNumContext::new().unwrap();

  // This is 65 bytes long with a prefix of 0x04...
  let pub_key_bytes = ec_pubkey.to_bytes(&group, PointConversionForm::UNCOMPRESSED, &mut ctx).unwrap();


  let enc_dates: u32 = ((((not_before.year() - 2000) % 100) as u32) << 19) |
    (not_before.month() << 15) |
    (not_before.day() << 10) |
    (not_before.hour() << 5) |
    EXPIRE_YEAR as u32;

  let enc_dates_bytes: [u8; 4] = unsafe { transmute(enc_dates.to_be()) };


  let hash_value = [&pub_key_bytes.as_slice()[1..], &enc_dates_bytes[1..]].concat();

  let mut sn_hash = hash(MessageDigest::sha256(), hash_value.as_slice()).unwrap();
  sn_hash[0] &= 0x7F;
  sn_hash[0] |= 0x40;

  BigNum::from_slice(&sn_hash[0..16]).unwrap()
}

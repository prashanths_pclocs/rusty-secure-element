mod bindings;
mod errors;
mod cert_defs;

use bindings::*;
use errors::*;
use cert_defs::*;


const IGNORE_MODULE_NOT_FOUND_ERRORS: bool = true;


fn handle_atca_status(status: ATCA_STATUS) -> Result<(), AtcaError> {
  match status {
    0 => Ok(()),
    240 => if IGNORE_MODULE_NOT_FOUND_ERRORS { Ok(()) } else { Err(AtcaError(240)) },
    n => Err(AtcaError(n)),
  }
}



pub fn init() -> Result<(), AtcaError> {
  let config = Box::new(ATCAIfaceCfg {
    iface_type: ATCAIfaceType_ATCA_I2C_IFACE,
    devtype: ATCADeviceType_ATECC608A,
    wake_delay: 1500,
    rx_retries: 20,
    __bindgen_anon_1: ATCAIfaceCfg__bindgen_ty_1 {
      atcai2c: ATCAIfaceCfg__bindgen_ty_1__bindgen_ty_1 {
        address: 0xC0,
        bus: 1,
        baud: 100000
      }
    },
    cfg_data: std::ptr::null_mut(),
  });
  handle_atca_status(unsafe { atcab_init(Box::into_raw(config)) })
}


pub fn setup() -> Result<(), AtcaError> {
  let mut is_locked = false;
  handle_atca_status(unsafe { atcab_is_config_locked(&mut is_locked) })?;

  println!("Module is setup? {}", is_locked);
  Ok(())
}



pub fn flash_cert(cert: &[u8]) {

  match unsafe { atcacert_write_cert(&DEVICE_CERT_DEF, cert.as_ptr(), cert.len()) } {
    0 => println!("Flashed cert successfully."),
    240 => println!("Could not contact the module."),
    9 => println!("Element out of bounds."),
    n => println!("Unknown error! {}", n),
  };
}


pub fn create_csr() -> Result<Vec<u8>, AtcaError> {
  let mut csr_data = [0u8; 1000];
  let mut csr_data_size = csr_data.len();
  let status = unsafe { atcacert_create_csr_pem(&DEVICE_CSR_DEF, csr_data.as_mut_ptr(), &mut csr_data_size) };

  println!("Certificate request has been generated with status {} and is... {} long", status, csr_data_size);

  match status {
    0 => Ok(csr_data[0..csr_data_size].to_vec()),
    _ => Err(AtcaError(status.try_into().unwrap())),
  }
}


pub fn write_cert(cert_der: Vec<u8>) -> Result<(), AtcaError> {
  let ptr = cert_der.as_slice().as_ptr();
  let status = unsafe { atcacert_write_cert(&DEVICE_CERT_DEF, ptr, cert_der.len()) };
  // TODO: This is an atcacert error if it is one...
  handle_atca_status(status.try_into().unwrap())
}

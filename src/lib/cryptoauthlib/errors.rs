use super::bindings::*;
use std::fmt;
use std::error::Error;

#[derive(Debug)]
pub struct AtcaError(pub ATCA_STATUS);

impl fmt::Display for AtcaError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let v = match self.0 {
      0x01 => "ATCA_CONFIG_ZONE_LOCKED",
      0x02 => "ATCA_DATA_ZONE_LOCKED",
      0xD0 => "ATCA_WAKE_FAILED | ATCA_INVALID_POINTER | ATCA_INVALID_LENGTH",
      0xD1 => "ATCA_CHECKMAC_VERIFY_FAILED",
      0xD2 => "ATCA_PARSE_ERROR",
      0xD4 => "ATCA_STATUS_CRC",
      0xD5 => "ATCA_STATUS_UNKNOWN",
      0xD6 => "ATCA_STATUS_ECC",
      0xD7 => "ATCA_STATUS_SELFTEST_ERROR",
      0xE0 => "ATCA_FUNC_FAIL",
      0xE1 => "ATCA_GEN_FAIL",
      0xE2 => "ATCA_BAD_PARAM",
      0xE3 => "ATCA_INVALID_ID",
      0xE4 => "ATCA_INVALID_SIZE",
      0xE5 => "ATCA_RX_CRC_ERROR",
      0xE6 => "ATCA_RX_FAIL",
      0xE7 => "ATCA_RX_NO_RESPONSE",
      0xE8 => "ATCA_RESYNC_WITH_WAKEUP",
      0xE9 => "ATCA_PARITY_ERROR",
      0xEA => "ATCA_TX_TIMEOUT",
      0xEB => "ATCA_RX_TIMEOUT",
      0xEC => "ATCA_TOO_MANY_COMM_RETRIES",
      0xED => "ATCA_SMALL_BUFFER",
      0xF0 => "ATCA_COMM_FAIL",
      0xF1 => "ATCA_TIMEOUT",
      0xF2 => "ATCA_BAD_OPCODE",
      0xF3 => "ATCA_WAKE_SUCCESS",
      0xF4 => "ATCA_EXECUTION_ERROR",
      0xF5 => "ATCA_UNIMPLEMENTED",
      0xF6 => "ATCA_ASSERT_FAILURE",
      0xF7 => "ATCA_TX_FAIL",
      0xF8 => "ATCA_NOT_LOCKED",
      0xF9 => "ATCA_NO_DEVICES",
      0xFA => "ATCA_HEALTH_TEST_ERROR",
      0xFB => "ATCA_ALLOC_FAILURE",
      0xFC => "ATCA_USE_FLAGS_CONSUMED",
      0xFD => "ATCA_NOT_INITIALIZED",
      _ => "Unknown error"
    };
    write!(f, "{}", v)
  }
}

impl Error for AtcaError {}




#[derive(Debug)]
pub struct AtcaCertError(pub std::os::raw::c_int);

impl fmt::Display for AtcaCertError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let v = match self.0 {
      0 => "ATCACERT_E_SUCCESS",
      1 => "ATCACERT_E_ERROR",
      2 => "ATCACERT_E_BAD_PARAMS",
      3 => "ATCACERT_E_BUFFER_TOO_SMALL",
      4 => "ATCACERT_E_DECODING_ERROR",
      5 => "ATCACERT_E_INVALID_DATE",
      6 => "ATCACERT_E_UNIMPLEMENTED",
      7 => "ATCACERT_E_UNEXPECTED_ELEM_SIZE",
      8 => "ATCACERT_E_ELEM_MISSING",
      9 => "ATCACERT_E_ELEM_OUT_OF_BOUNDS",
      10 => "ATCACERT_E_BAD_CERT",
      11 => "ATCACERT_E_WRONG_CERT_DEF",
      12 => "ATCACERT_E_VERIFY_FAILED",
      13 => "ATCACERT_E_INVALID_TRANSFORM",
      _ => "Unknown error"
    };
    write!(f, "{}", v)
  }
}

impl Error for AtcaCertError {}



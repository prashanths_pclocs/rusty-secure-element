use std::path::PathBuf;
use std::fs;
use openssl::{x509::{X509, X509Req}, ec::EcKey};
use secure_element::{cryptoauthlib::{create_csr, write_cert}, certgen::generate_cert};



pub fn configure() -> Result<(), ()> {
  todo!("Implement this!");
}



pub fn setup_cert(ca_key: &PathBuf, ca_crt: &PathBuf) -> Result<(), ()> {

  let ca_key = EcKey::private_key_from_pem(fs::read_to_string(ca_key).unwrap().as_bytes()).unwrap();
  let ca_crt = X509::from_pem(fs::read_to_string(ca_crt).unwrap().as_bytes()).unwrap();

  let csr_pem = create_csr();


  let csr = if csr_pem.is_ok() {
    X509Req::from_pem(&csr_pem.unwrap()[..]).unwrap()
  } else {
    println!("Failled back to hard coded CSR for dev...");
    X509Req::from_pem(fs::read_to_string("test_files/device.csr.pem").unwrap().as_bytes()).unwrap()
  };
  println!("Done creating the CSR");


  let device_cert = generate_cert("70:b3:d5:8f:9d:27_01236f3af2e99864ee", csr, ca_crt, ca_key);

  let device_cert_pem = device_cert.to_pem().unwrap();
  let device_cert_pem_str = std::str::from_utf8(device_cert_pem.as_slice()).unwrap();
  println!("Device cert: {}", device_cert_pem_str);


  write_cert(device_cert.to_der().unwrap()).unwrap();
  println!("Wrote device cert");

  Ok(())
}

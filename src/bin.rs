use std::path::PathBuf;
use clap::{Parser, Subcommand};
use secure_element::cryptoauthlib;

mod commands;


#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
  #[command(subcommand)]
  command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
  // Configure the secure element for usage
  Configure {
    /// lock the config permenantly
    lock_config: Option<bool>,
  },

  SetupCert {
    /// path to a ca key as pem
    ca_key: PathBuf,
    /// path to a ca crt as pem
    ca_crt: PathBuf,
  },

  /// Flash a certificate onto the secure element
  FlashCerts {
    /// a pem encoded certificate file
    cert_file: PathBuf,
  },
}




fn main() {
  let cli = Cli::parse();

  cryptoauthlib::init().unwrap();

  match &cli.command {
    Some(Commands::Configure { lock_config }) => {
      println!("We will make sure the secure element is ready for use. Locking config? {}", lock_config.unwrap_or(false));
      let _ = commands::configure();
    }
    Some(Commands::SetupCert { ca_key, ca_crt }) => {
      println!("Generating cert onto the device for the given ca.");
      commands::setup_cert(ca_key, ca_crt).unwrap();
    },

    Some(Commands::FlashCerts { cert_file }) => {
      println!("We will flash the certificate from `{}` into the secure element now.", cert_file.as_path().display());
    }
    None => {}
  }

}

